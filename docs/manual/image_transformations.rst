.. _image_transformations:

.. _PyJAMAS: https://bitbucket.org/rfg_lab/pyjamas/src/master/

=====================
Image transformations
=====================

Images can be transformed (e.g. cropped or registered) using annotations:

#. To crop an image, draw a rectangle around the region of the image to be preserved, and select *Crop* from the **Image** menu.

   It is possible to crop an image using any type of polyline: the bounding box of the polyline will be used if the polyline is not rectangular.

        .. image:: ../images/crop.gif
            :width: 75%
            :align: center

   If there is more than one polyline on the slice being displayed and the *Crop* option is selected, the polyline whose id is 1 will be used to determine the region to crop.

#. To register the slices (Z planes or consecutive time points) that form an image sequence, add a constant number of fiducials to the slices to be registered, making sure that the fiducial ids of corresponding structures match. Move to the slice that will be the reference for image registration and select the *Register* option from the **Image** menu.

        .. image:: ../images/register.gif
            :width: 75%
            :align: center

   To speed up the generation of fiducials for image registration, add fiducials to image features on the first image (manually or using *Find seeds ...*). Project the fiducials onto subsequent time points using the *Propagate seeds ...* option in the **Image** menu.

        .. image:: ../images/propagate_seeds.gif
            :width: 75%
            :align: center

   The registration algorithm in PyJAMAS_ will shift images by the mean x and y displacements of the fiducials with respect to the corresponding fiducials in the reference image.

   If fiducial ids do not match, PyJAMAS_ provides a tool to automatically track fiducials and match their ids. Select *Track fiducials ...*  under the **Annotations** menu. For each fiducial in the source slice, the algorithm will determine the closest fiducial in the target (next) slice. If two fiducials from the source slice are mapped on to the same fiducial in the target slice, PyJAMAS_ will produce an error indicating the ids of the overlapping fiducials. The same number of fiducials should be present on each slice for this simple tracking algorithm to work.

        .. image:: ../images/track_fiducials.gif
            :width: 75%
            :align: center

