.. _handling_images:

.. _PyJAMAS: https://bitbucket.org/rfg_lab/pyjamas/src/master/

===============
Handling images
===============

#. To **open a grayscale image** (including multipage tiff files), use the *Load grayscale image ...* option from the **IO** menu.

    .. image:: ../images/load_grayscale.png
        :width: 55%
        :align: center

#. To **save the image currently open** (not including annotations, but including any changes to the pixel values), use the *Save grayscale image ...* option from the **IO** menu.

    .. image:: ../images/save_grayscale.png
        :width: 55%
        :align: center

#. To **save the current image, including annotations**, use *Save display ...* (single image) or *Export movie with annotations ...* (all images in a sequence) from the **IO** menu.

    .. image:: ../images/save_display.png
        :width: 55%
        :align: center

#. PyJAMAS_ provides options to **rotate, flip, invert, and project images, or to play time-lapse sequences**, using the corresponding options under the **Image** menu.

    .. image:: ../images/rotate.gif
        :width: 49%

    .. image:: ../images/flip.gif
        :width: 49%

    .. image:: ../images/max_project.gif
        :width: 49%

    .. image:: ../images/sum_project.gif
        :width: 49%

    .. image:: ../images/invert.gif
        :width: 49%

    .. image:: ../images/play.gif
        :width: 49%


#. The option to *Undo image operation ...* reverts back to the previous image. By default, PyJAMAS_ will store the five most recent versions of the image currently open.

    .. image:: ../images/undo.png
        :width: 55%
        :align: center
