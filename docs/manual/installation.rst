.. _installation:

.. _pip: https://pip.pypa.io
.. _PyJAMAS: https://bitbucket.org/rfg_lab/pyjamas/src/master/
.. _Python: https://www.python.org/downloads/
.. _Shapely: https://www.lfd.uci.edu/~gohlke/pythonlibs/#shapely

============
Installation
============

#. Install Python_ 3.6 or above.

   PyJAMAS_ uses type annotations internally to improve code readability. Type annotations provide a syntax to specify the data types of variables in Python_. Type annotations are only available from Python_ 3.6, and thus, PyJAMAS_ will not work with earlier Python_ versions.

#. Download and install PyJAMAS_ by opening a terminal and typing:

   .. code-block:: bash

    $ python3 -m pip install --no-cache-dir -U pyjamas-rfglab

   pip_ is a package manager for Python. pip_ is typically installed with Python_, but you can also install pip_ following these `instructions. <https://pip.pypa.io/en/stable/installing/>`_.

   After installing pip_, it is good practice to update it. To update pip_, open a terminal and type:

   .. code-block:: bash

    $ python3 –m pip install –U pip

   substituting **python3** with the name of the Python_ executable (typically **python3** in MacOS and Linux, and **python** in Windows).

   **NOTE FOR WINDOWS USERS**

   PyJAMAS_ uses the package Shapely_ to represent geometrical objects, such as points or polygons. Shapely_ is automatically installed with PyJAMAS_ in MacOS and Linux. However, the installation needs to be done manually in Windows, after installing Python_ and pip_, and before installing PyJAMAS_.

   To install Shapely_, download the appropriate version from this site: https://www.lfd.uci.edu/~gohlke/pythonlibs/#shapely. For example, use Shapely‑1.6.4.post2‑cp37‑cp37m‑win_amd64.whl for a 64-bit machine running Python 3.7.

   Open a terminal and navigate to the folder that contains the downloaded .whl file using the cd command.

   Complete the installation of Shapely by typing:

   .. code-block:: bash

    $ python -m pip install Shapely‑1.6.4.post2‑cp37‑cp37m‑win_amd64.whl

   substituting the downloaded file name as appropriate.

   **NOTES FOR LINUX USERS**

   For Linux users, we strongly suggest using the most recent version of Python 3.8, which solves incompatibilities with Theano, a machine learning package used in PyJAMAS_.

   In Linux systems, you will need permissions to install PyJAMAS_ globally. To restrict the PyJAMAS_ installation to the current user, install it with:

   .. code-block:: bash

    python3 –m pip install --user --no-cache-dir -U pyjamas-rfglab

#. To run PyJAMAS_, open a terminal and type:

   .. code-block:: bash

    $ pyjamas

#. The code for PyJAMAS_ can be found in the PyJAMAS_ folder under the Python_ site packages (e.g. /usr/local/lib/python3.7/site-packages in MacOS). The location of the source code is important to extend PyJAMAS_ using `plugins <plugins.html>`_. Alternatively, you can download the PyJAMAS_ source code from: https://bitbucket.org/rfg_lab/pyjamas/src/master/.

#. PyJAMAS can be run from the source code by opening a terminal, navigating to the folder that contains the code, and typing:

   .. code-block:: bash

    $ python3 pjscore.py

